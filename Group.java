package homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Group {

	private String groupName;
	private List<Student> students = new ArrayList<>(10);

	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {

		for (Student stud : students) {
			if (stud != null) {
				if (stud.getLastName().equals(lastName)) {
					return stud;
				}
			}
		}
		throw new StudentNotFoundException();

	}

	public boolean removeStudentByID(int id) {

		for (Student stud : students) {

			if (stud.getId() == id) {
				students.remove(stud);
				return true;
			}

		}
		return false;

	}

	public void addStudent(Student student) throws GroupOverflowException {

		if (students.size() < 10) {
			students.add(student);
			student.setGroupName(this.groupName);
			return;

		}

		throw new GroupOverflowException("Group is full");

	}

	public Group(String groupName, ArrayList<Student> students) {
		super();
		this.groupName = groupName;
		this.students = students;
	}

	public Group() {
		super();
	}

	public String getGroupName() {
		return groupName;
	}

	public Group(String groupName) {
		super();
		this.groupName = groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(ArrayList<Student> students) {
		this.students = students;
	}

	public void sortStudentsByLastName() {
		Collections.sort(students, Comparator.nullsLast(new StudentsNameComparator()));
	}

	public void equivaltStudents() {
		boolean result = false;
		for (int i = 0; i < students.size() - 1; i++) {
			for (int j = i + 1; j < students.size(); j++) {
				if (students.get(i).equals(students.get(j))) {

					result = true;
					break;

				}
			}
		}
		if (result) {
			System.out.println("Group " + groupName + " has duplicated entries of type Student");
		} else {
			System.out.println("Group " + groupName + " doesn't have duplicated students");
		}
	}

	@Override
	public String toString() {
		sortStudentsByLastName();

		String group = "Group " + groupName + ":";

		for (int i = 0; i < students.size(); i++) {
			group = group + students.get(i);
		}

		return group;
	}

	@Override
	public int hashCode() {
		return Objects.hash(groupName, students);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		return Objects.equals(groupName, other.groupName) && Objects.equals(students, other.students);
	}

}