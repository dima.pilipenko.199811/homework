package homework;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		Dictionary translator = new Dictionary();
		File file1 = new File("EnglishIn.txt");
		File file2 = new File("UkrainianOut.txt");
		translator.interactiveAdd();
		
		try (PrintWriter pw = new PrintWriter(file1)) {

			pw.println("Cat came to me.");

		} catch (IOException e) {
			e.printStackTrace();
		}

		
		translator.translate(file1,file2);
		
		
		
		File file3 = new File("Save Dictionary.txt");
        try {
            file3.createNewFile();
            translator.saveTranslator(file3);
        }catch (IOException e){
            e.printStackTrace();
        }
	}

}
