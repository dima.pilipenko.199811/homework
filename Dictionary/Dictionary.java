package homework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;



public class Dictionary {
	 private Map<String,String> dict = new HashMap<>();

	    public Dictionary() {
	        this.dict = dict;
	        words();
	    }

	    public Map<String, String> getDictionary() {
	        return dict;
	    }
	

	public void words() {
		dict.put("Cat", "кіт");
		dict.put("came", "прийшов");
		dict.put("to", "до");
		dict.put("me", "мене");
	}

	public void translate(File fileIn, File fileOut) {

		if (fileIn == null || fileOut == null) {
			throw new IllegalArgumentException();
		}
		StringBuilder sb = new StringBuilder();
		String englishWords = getWordsFromFile(fileIn);
		String[] englishArray = getAraayWords(englishWords);
		Set<String> keys = dict.keySet();
		for (String s : englishArray) {
			for (String key : keys) {
				if (s.equals(key)) {
					sb.append(dict.get(key) + " ");
				}

			}
		}
		saveTranslate(sb.toString(), fileOut);
	}

	public static String getWordsFromFile(File fileIn) {
		if(fileIn == null){
            throw  new IllegalArgumentException();
        }
        StringBuilder sb = new StringBuilder();
        try(BufferedReader br = new BufferedReader(new FileReader(fileIn))){
            String text = "";
            for(;(text = br.readLine()) != null;){
                sb.append(text);
                sb.append(System.lineSeparator());
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return sb.toString();
    }
	
	 private String[] getAraayWords(String wordsText){
	        String[] wordsArra = wordsText.split("[,;:.!?\\s]+");
	        return wordsArra;
	    }

	private void saveTranslate(String text, File clearFile) {

		try (PrintWriter pw = new PrintWriter(clearFile)) {
			pw.print(text);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void saveTranslator(File fileDictionary) throws IOException{
        if(fileDictionary == null){
            throw new IllegalArgumentException();
        }
        StringBuilder sb = new StringBuilder();
        Set<String> keys = dict.keySet();
        for(String key: keys){
            sb.append(key+" - "+dict.get(key));
            sb.append(System.lineSeparator());
        }
        saveTranslate(sb.toString(),fileDictionary);
    }
	
	public void interactiveAdd(){
        String wordsEng = "";
        String wordsTrans = "";
        Scanner sc = new Scanner (System.in);
        
        System.out.println("Input words in English");
        wordsEng = sc.next();
        System.out.println("Input translate");
        wordsTrans = sc.next();
        dict.put(wordsEng,wordsTrans);
    }
	
	@Override
    public String toString() {
        return "Translator{" +
                "dictionary=" + dict +
                '}';
    }

}
