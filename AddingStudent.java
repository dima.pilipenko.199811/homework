package homework;

import java.util.Scanner;

public class AddingStudent {
	
	Scanner sc = new Scanner(System.in);
	
	public Student newStudent() {
		try {
			
		Student student = new Student();
		System.out.println("Enter name of student");
		student.setName(sc.next());
		System.out.println("Enter last name of student");
		student.setLastName(sc.next());
		System.out.println("Enter gender(MALE/FEMALE) of student");
		student.setGender(Gender.valueOf(sc.next()));
		System.out.println("Enter id of student");
		student.setId(sc.nextInt());
		System.out.println("Enter group of student");
		student.setGroupName(sc.next());
		return student;
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Wrong input");
			return null;
		}
	} 


}
