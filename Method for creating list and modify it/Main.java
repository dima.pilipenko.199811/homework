package homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

	public static void main(String[] args) {

		listCreatingAndModify();

	}

	public static void listCreatingAndModify() {

		Random random = new Random();
		List <Integer> list = new ArrayList<Integer>(10);

		for (int i = 0; i < 10; i++) {
			list.add(i, random.nextInt(11));
		}
		System.out.println(list);

		list.remove(9);
		list.remove(0);
		list.remove(0);

		System.out.println(list);

	}

}
