package homework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CopyFilesByType {
	
	public static long copyFile(File fileIn, File fileOut) throws IOException {

		try (InputStream is = new FileInputStream(fileIn); OutputStream os = new FileOutputStream(fileOut)) {

			return is.transferTo(os);

		}

	}
	
	public static void copyFilesByType(String folderIn, String type, String folderOut) {
        File folder1 = new File(folderIn);
        File folder2 = new File(folderOut);
        if(!folder1.exists()) System.out.println(folderIn + " папка не существует");
        File[] listFiles = folder1.listFiles(new FileNameFilter(type));
        if(listFiles.length == 0){
            System.out.println(folderIn + " не содержит файлов с расширением " + type);
        }else{
        	for (int i = 0; i < listFiles.length; i++) {
    			if (listFiles[i].isFile()) {
    				File fileOut = new File(folderOut, listFiles[i].getName());
    				try { copyFile(listFiles[i], fileOut);
						
					} catch (Exception e) {
						// TODO: handle exception
					} 
    		

    			}
    		}
    		
        }
    }
	
	

}
