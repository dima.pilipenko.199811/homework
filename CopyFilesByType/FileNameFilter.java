package homework;

import java.io.File;
import java.io.FilenameFilter;

public class FileNameFilter implements FilenameFilter{
	
        
        private String type;
         
        public FileNameFilter(String type){
            this.type = type.toLowerCase();
        }
        public boolean accept(File fromFolder, String name) {
            return name.toLowerCase().endsWith(type);
        }
    }


