package homework;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class GroupFileStorage {

	public void saveGroupToCSV(Group gr) {

		File group = new File("E:\\projectJava\\" + gr.getGroupName() + ".csv");
		Student[] students = gr.getStudents();
		String result = "";

		try (PrintWriter pw = new PrintWriter(group)) {
			for (int i = 0; i < students.length; i++) {
				if (students[i] != null) {
					result += CSVConvector.toStringRepresentation(students[i]) + System.lineSeparator();
				}
			}
			pw.print(result);
		} catch (FileNotFoundException e) {

		}

	}

	public Group loadGroupFromCSV(File file) {
		
		Group group = new Group(file.getName().substring(0, file.getName().indexOf(".")));
		try (Scanner sc = new Scanner(file)) {
			while (sc.hasNextLine()) {
				group.addStudent(CSVConvector.fromStringRepresentation(sc.nextLine()));
			}
		} catch (FileNotFoundException e) {
			
		} catch (GroupOverflowException e) {
			
		}
		return group;
	}
		
		


	public File findFileByGroupName(String groupName, File folder) {
		if (folder.isDirectory()) {
			File[] files = folder.listFiles();

			for (int i = 0; i < files.length; i++) {
				if (files[i].getName().contains(groupName)) {
					return files[i].getAbsoluteFile();
				}
			}
		}
		return null;

	}
}
