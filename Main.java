package homework;



public class Main {

	public static void main(String[] args) {
		

		Student student1 = new Student("Oleg", "Petrenko", Gender.MALE, 1, "Java");
		Student student2 = new Student("Dima", "Lomachenko", Gender.MALE, 2, "Java");
		Student student3 = new Student("Diana", "Litvichenko", Gender.FEMALE, 3, "Java");
		Student student4 = new Student("Olena", "Ponimatchenko", Gender.FEMALE, 4, "Java");
		Student student5 = new Student("Victor", "Pavlik", Gender.MALE, 5, "Java");
		Student student6 = new Student("Petro", "Schur", Gender.MALE, 6, "Java");
		Student student7 = new Student("Igor", "Kondratyk", Gender.MALE, 7, "Java");
		Student student8 = new Student("Oleg", "Vinnik", Gender.MALE, 8, "Java");
		Student student9 = new Student("Stepan", "Giga", Gender.MALE, 9, "Python");
		Student student10 = new Student("Darina", "Astafieva", Gender.FEMALE, 10, "Python");
		Student student11 = new Student("Olga", "Kravchenko", Gender.FEMALE, 11, "Python");

		Group java = new Group("Java");
		Group python = new Group("Python");

		try {
			java.addStudent(student1);
			java.addStudent(student2);
			java.addStudent(student3);
			java.addStudent(student4);
			java.addStudent(student5);
			java.addStudent(student6);
			java.addStudent(student7);
			java.addStudent(student8);
			python.addStudent(student9);
			python.addStudent(student10);
			python.addStudent(student11);

		} catch (GroupOverflowException e) {
			e.printStackTrace();
			System.out.println("Group is full");

		}

		System.out.println(java);
		System.out.println(python);

		try {
			Student exile = java.searchStudentByLastName("Litvichenko");
			System.out.println("Student is found:" + exile);
		} catch (StudentNotFoundException e) {
			e.printStackTrace();
		}
		
		AddingStudent student12 = new AddingStudent();
		
		try {
			java.addStudent(student12.newStudent());
		} catch (GroupOverflowException e) {
			e.printStackTrace();
		}
		
		System.out.println(java);
	

	}
}
